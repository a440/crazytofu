<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeonamesRanked extends Migration
{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
				Schema::create('geonames_ranked', function (Blueprint $table) {
					$table->integer('geonameid');
					$table->string('name', 256);
					$table->string('asciiname', 256)->nullable();
					$table->text('alternatenames')->nullable();
					$table->float('latitude');
					$table->float('longitude');
					$table->string('fclass', 1)->nullable();
					$table->string('fcode', 10)->nullable();
					$table->string('country_code', 2)->nullable();
					$table->string('admin1', 20)->nullable();
					$table->string('admin2', 80)->nullable();
					$table->string('admin3', 20)->nullable();
					$table->string('admin4', 20)->nullable();
					$table->bigInteger('population');
					$table->string('area', 256)->nullable();
					$table->string('borough', 256)->nullable();
					$table->string('city', 256)->nullable();
					$table->string('region', 256)->nullable();
					$table->string('country', 256)->nullable();
					$table->float('rank')->nullable();
					$table->bigInteger('rank_int')->nullable();

					$table->primary('geonameid');
				});

				DB::statement("CREATE INDEX geonames_ranked_fts_index ON geonames_ranked USING gin (to_tsvector('simple'::regconfig, (name || ' ' || asciiname || ' ' || alternatenames || ' ' || city || ' ' || country)::text));");
				DB::statement("CREATE INDEX geonames_ranked_rank_int ON geonames_ranked (rank_int NULLS LAST);");
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
				Schema::drop('geonames_ranked');
		}
}
