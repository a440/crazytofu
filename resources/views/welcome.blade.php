<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/jumbotron-narrow/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>CrazyTofu</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="http://moodaway.a440.io">Documentation</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">CrazyTofu</h3>
      </div>

      <div class="jumbotron">
        <h1>Try CrazyTofu here!</h1>
        <div class="row">
          <div class="col-lg-4"><p>Search for stuff:</p></div>
          <div><input class="col-lg-8" type="text" id="crazytofu"></div>
				</div>
        <p><a class="btn btn-lg btn-success" href="http://getbootstrap.com/examples/jumbotron-narrow/#" role="button">I don't get it</a></p>
      </div>

      <footer class="footer">
        <p>© A440 2015</p>
      </footer>

    </div> <!-- /container -->
		<script src="./jquery-1.11.3.min.js"></script>
		<script src="./jquery.autocomplete.min.js"></script>
		<script type="text/javascript">
		$('#crazytofu').autocomplete({
			serviceUrl: '/places',
			paramName: 'term',
			transformResult: function(response) {
				return {
					query: 'Unit',
					suggestions: $.map(JSON.parse(response), function(n,i) {
								return n.name + ', ' + n.surroundingArea;
						})
				}
			}
		});
		</script>
		<style style="text/css">
		.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }

</style>
</body></html>
