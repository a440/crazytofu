<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaginableRequest;
use App\Http\Requests\PlacesListRequest;

use Illuminate\Http\Request;

use App\Models\Place;

use Illuminate\Support\Facades\Cache;

use App\Transformers\PlaceTransformer;

class PlacesController extends Controller
{
	/**
	 * Places listing
	 *
	 * <code>
	 * curl -X "GET" "http://crazytofu.local/places?term=Pari"
	 * </code>
	 *
	 * @param  Request  $request
	 * @return Response
	 */
		public function index(PlacesListRequest $request)
		{
				$cache_key = 'places_fts_'.implode($request->all());

				if (Cache::has($cache_key)) {
						$places = Cache::get($cache_key);
				} else {
						$places = Place::orderBy('rank_int')
						->withNameLike($request->input('term'))
						->get();

						$places = $places->slice(0, $request->get('per_page', 10));
						Cache::put($cache_key, $places, config('geosearch.cachetime'));
				}

				return $places;
		}
}
