<?php namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\ValidationException;

use Illuminate\Contracts\Validation\Validator;

use App\Event;

class PlacesListRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'term' => 'required|string|between:1,16'
		];
	}

	/**
	* Handle a failed validation attempt.
	*
	* @param  \Illuminate\Contracts\Validation\Validator  $validator
	* @return mixed
	*/
	protected function failedValidation(Validator $validator)
	{
			throw new ValidationException($validator);
	}
}
