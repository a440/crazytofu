<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'geonames_ranked';

    protected $visible = [
      'id',
      'name',
      'latitude',
      'longitude',
      'surroundingArea'
    ];

    protected $appends = [
      'id',
      'surroundingArea'
    ];

    public function getIdAttribute() {
      return $this->geonameid;
    }

    public function getSurroundingAreaAttribute() {
      $areas = [
        $this->city,
        $this->region,
        $this->country
      ];

      return implode(', ', array_filter($areas));
    }

    public function scopeWithNameLike($query, $like)
    {
        $pdo = \DB::connection()->getPdo();

        if (!is_null($like) && strlen($like) != 0) {
            return $query->whereRaw('to_tsvector(\'simple\', (name || \' \' || asciiname || \' \' || alternatenames || \' \' || city || \' \' || country)::text) @@ to_tsquery(\'simple\',\'\''.$pdo->quote($like).'\':*\')', [  ]);
        } else {
            return $query;
        }
    }
}
