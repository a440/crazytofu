<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use libphonenumber\NumberParseException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Contracts\Validation\ValidationException;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		//'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if($e instanceof ValidationException) {
			return new JsonResponse(['error' => ['ValidationException' => [$e->getMessageProvider()->getMessageBag()->getMessages()]]] , 400);
		}

		if($e instanceof ModelNotFoundException) {
			return new JsonResponse(['error' => ['ModelNotFoundException' => [$e->getMessage()]]], 404);
		}

		if($e instanceof NumberParseException) {
			return new JsonResponse(['error' => ['NumberParseException' => [$e->getMessage()]]], 400);
		}

		if($e instanceof MethodNotAllowedHttpException) {
			return new JsonResponse(['error' => ['MethodNotAllowedHttpException' => ['this route does not exist mate']]], 405);
		}

		if($e instanceof NotFoundHttpException) {
			return new JsonResponse(['error' => ['NotFoundHttpException' => ['this route does not exist mate']]], 404);
		}

		if($e instanceof QueryException) {
			return new JsonResponse(['error' => ['QueryException' => ['']]], 500);
		}

		if($e instanceof Exception) {
			//return new JsonResponse(['error' => ['Exception' => [$e->getMessage()]]], 500);
		}

		return parent::render($request, $e);
	}

}
